---
layout: post
title:  "Linux Kernel - Eudyptula Challenge"
date:   2013-05-05
categories: article
title-image: tux.png
---

Eudyptula Challenge (where have you been all my life?)

## Challenge 1 - Email and all that

Great testing ground for email clients.


## Challenge 2 - Patching and building

Unicode kernel image names... danger :D

## Challenge 3 - Submitting patches

My patches...
