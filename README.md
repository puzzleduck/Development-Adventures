# Development-Adventures
Development adventures blog for PuZZleDucK.org hosted on GitLab Pages at https://puzzleduck.gitlab.io/Development-Adventures/

Development and deployment occurs on the gh-pages branch

# Setup

```bash
git clone https://gitlab.com/puzzleduck/Development-Adventures.git
bundle update
cd Development-Adventures
bundle exec jekyll serve
```
